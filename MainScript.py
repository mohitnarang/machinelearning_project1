# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 22:01:07 2015

@author: mohitnarang
"""


from sklearn import linear_model 
from sklearn import metrics
from sklearn import preprocessing
#from sklearn import cross_validation
#from sklearn import preprocessing as pp
# import logloss
import numpy as np
import time
import random
#import ProjectScoring as ps


print "Starting to read datasets: ", time.localtime(), "\n"
# create the training and test sets, skipping the header row with [1:]
dataset = preprocessing.normalize( np.genfromtxt('train_processed.csv', delimiter=',',skip_header=1))



training_samples = random.sample(range(1,667),500)
validate_samples = list(set(range(1,667))-set(training_samples))

train = dataset[training_samples,0:17]
label = dataset[training_samples,17]



validate = dataset[validate_samples,0:17]
validate_label = dataset[validate_samples,17]

testset = np.genfromtxt('validate_and_test_processed.csv',delimiter=',')
test = testset[:,:]


regressor = linear_model.LinearRegression()
# Train the model
regressor.fit(train,label)
# Test values
predictions = regressor.predict(validate)

# Calculate Score
score_linearregression  = metrics.mean_squared_error(validate_label,predictions)

regressor_ridge = linear_model.Ridge() 
regressor_ridge.fit(train,label)
predictions_ridge = regressor_ridge.predict(validate)

score_ridgeregression = metrics.mean_squared_error(validate_label,predictions_ridge)

regressor_lasso = linear_model.LassoCV(eps=.0000001,positive=True) 
regressor_lasso.fit(train,label)
predictions_lasso = regressor_lasso.predict(validate)

score_lassoregression = metrics.mean_squared_error(validate_label,predictions_lasso)

final_linear = regressor.predict(testset)
final_ridge = regressor_ridge.predict(testset)
final_lasso = regressor_lasso.predict(testset)

np.savetxt('final_Lasso_1e_minus_7.csv',final_lasso,delimiter=',',fmt="%d")
